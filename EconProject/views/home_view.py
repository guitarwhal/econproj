import os
from flask import Blueprint, current_app, flash, redirect, render_template, request, url_for

bp = Blueprint('home', __name__, url_prefix='/')

@bp.route('/')
def home():
    return render_template("index.html")