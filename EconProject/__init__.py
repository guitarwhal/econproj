import os
from flask import Flask, render_template, current_app
from flask_login import LoginManager
import secrets

from .views.home_view import bp as home_bp

def create_app(test_config=None):
    app = Flask(__name__)

    app.config.from_mapping(
        SECRET_KEY=secrets.token_urlsafe(32),
    )
    
    app.register_blueprint(home_bp)
    
    return app
